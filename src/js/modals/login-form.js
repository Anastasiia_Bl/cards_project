import { Request } from "./requests-url.js";
import {VisitCard, visitCardCardiologist, visitCardDentist, visitCardTherapist} from "./visitCard.js";

// Закрити модальне вікно
export function closeModalForm() {
    let closeBtn = document.querySelector(".header-modal__btn-close");
    let modalForm = document.querySelector(".modal");
    let inputEmail = document.querySelector(".input__email");
    let inputPassword = document.querySelector(".input__password");
  
    closeBtn.addEventListener('click', ()=> {
        modalForm.classList.add('hidden');
        inputEmail.value = "";
        inputPassword.value = "";
    })
    document.addEventListener('click', (e) => {
      if(e.target.classList.contains('modal')) {
        modalForm.classList.add('hidden');
        inputEmail.value = "";
        inputPassword.value = "";
      }
    })
  }
  
  
  
  // Відкрити модальне вікно Логінізації
  export function openLoginForm() {
    let loginBtn = document.querySelector(".login-btn");
    let modalLoginForm = document.querySelector(".modal-login");
    loginBtn.addEventListener('click', ()=> {
        modalLoginForm.classList.remove('hidden');
    })
  }
  
  
  
  // Показати, сховати значення Пароля
  let showPass = document.querySelectorAll(".password__btn");
  showPass.forEach(item =>
    item.addEventListener('click', changeType)
  );
  
  export function changeType() {
    let input = document.querySelector('.input__password');
    let btnEyeGrey = document.querySelector('.eye-grey');
    let btnEyeBlue = document.querySelector('.eye-blue');
    if (input.type === 'password') {
        input.type = 'text';
        btnEyeGrey.style.display = "none";
        btnEyeBlue.style.display = "block";
    } else {
        input.type = 'password';
        btnEyeGrey.style.display = "block";
        btnEyeBlue.style.display = "none";
    }
  }
  
  
  // Функція перевірки інпутів в Логін-формі
  export function validateLogin({email, password}) {
    let emailError = document.querySelector(".input__email-error");
    let passwordError = document.querySelector(".input__password-error");
    let inputEmail = document.querySelector(".input__email");
    let inputPassword = document.querySelector(".input__password");
    inputEmail.addEventListener('focus', () => emailError.classList.add('hidden'));
    inputEmail.addEventListener('focus', () => inputEmail.classList.remove('input__error'));
    inputPassword.addEventListener('focus', () => passwordError.classList.add('hidden'));
    inputPassword.addEventListener('focus', () => inputPassword.classList.remove('input__error'));
  
    if (!email) {
        emailError.classList.remove('hidden');
        inputEmail.classList.add('input__error');
        return false;
      } 
  
    if (!password) {
        passwordError.classList.remove('hidden');
        inputPassword.classList.add('input__error');
        return false;
      }
      
    return true;
  }
  
  
  // Функція перевірки наявності проходження авторизації (токена)
  export function checkAuthorization() {
  const token = localStorage.getItem("token");
  if (token) {
    // Якщо токен є, ховаємо форму логіна і показуємо основний контент
    document.querySelector(".unauthorised").classList.add("hidden");
    document.querySelector(".filter").classList.remove("hidden");
    toggleButtons();
    visitCardsInit();
  } else {
    // Якщо токена немає, показуємо неавторизовану форму
    document.querySelector(".unauthorised").classList.remove("hidden");
    document.querySelector(".filter").classList.add("hidden");
  }
  }
  // Викликаємо ф-цію при завантажені сторінки
  
  
  export async function login(e) {
  const loginForm = document.getElementById("form-login");
  const email = loginForm.email.value.trim();
  const password = loginForm.password.value.trim();
  
  const loginData = {
    email: email,
    password: password,
  };
  console.log(loginData);
  
  if (!validateLogin(loginData)) {
    return false;
    // return alert('Try with correct email and password');
  }
  try {
    e.preventDefault();
    const response = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(loginData),
    });
  
    const token = await response.text();
    if (token === "Incorrect username or password") {
      return alert(token);
    } else { 
      document.querySelector(".modal-login").classList.add('hidden');
      document.querySelector(".unauthorised").classList.add('hidden')
      document.querySelector(".filter").classList.remove('hidden');
  
      setToken(token);
      toggleButtons();
      visitCardsInit();
    }
  } catch (error) {
    console.log(error);
  }
  }
  
  
  export function logInBtnClick() {
    const loginSubmit = document.querySelector("#login-form-btn");
  
    loginSubmit.addEventListener("click", (e) => {
    login(e);
    });
  }
  
  
  // зберегти токен
  export function setToken(token) {
  localStorage.setItem("token", token);
  }
  
  // отримати токен
  export function getToken() {
  return localStorage.getItem("token") ? localStorage.getItem("token") : null;
  }
  
  // зміна кнопок Хедера (логін і створити візит)
  export function toggleButtons() {
  let loginBtn = document.querySelector(".login-btn");
  let createVisitBtn = document.querySelector("#create-visit-btn");
  loginBtn.classList.toggle("hidden");
  createVisitBtn.classList.toggle("hidden");
  }
  
  // ініціалізувати картки
  export const cardsArray = [];
  export function visitCardsInit() {
    Request.getAllCards().then((visits) => {
    if (!visits.length) {
      document.querySelector('.authorise-block').classList.remove('hidden');
    }
    visits.forEach((visit) => {
      console.log(visit);
      document.querySelector('.cards-section').classList.remove('hidden');
      cardRender(visit);
    });
    return cardsArray;
  });
  }
  
  export function cardRender(obj) {
    let visitCard = {};
    switch (obj.doctor) {
      case "Cardiologist":
        visitCard = new visitCardCardiologist(
          obj.firstName,
          obj.lastName,
          obj.doctor,
          obj.purpose,
          obj.description,
          obj.priority,
          obj.id,
          obj.status,
          obj.pressure,
          obj.bmi,
          obj.illnesse,
          obj.age
        );
        break;
      case "Therapist":
        visitCard = new visitCardTherapist(
          obj.firstName,
          obj.lastName,
          obj.doctor,
          obj.purpose,
          obj.description,
          obj.priority,
          obj.id,
          obj.status,
          obj.age
        );
        break;
      case "Dentist":
        visitCard = new visitCardDentist(
          obj.firstName,
          obj.lastName,
          obj.doctor,
          obj.purpose,
          obj.description,
          obj.priority,
          obj.id,
          obj.status,
          obj.lastVisitDate
        );
    }
    console.log(visitCard)
    let cardsSection = document.querySelector('.cards-section');
    cardsSection.classList.remove('hidden');
    cardsArray.push(visitCard);
    const htmlCard = visitCard.createHTMLCard();
    console.log(htmlCard);
    cardsSection.append(htmlCard);
    visitCard.deleteCard(htmlCard);
    visitCard.showAdditionalInfo(htmlCard);
    visitCard.editCard(htmlCard);
  }
  