// поиск и замена
import replace from "gulp-replace";
// Обработка ошибок
import plumber from "gulp-plumber";
// Сообщения (подсказки) по ошибкам
import notify from "gulp-notify";
// локальный сервер для обновления, и видимости изменений
import browsersync from "browser-sync";
// проверка обновления (для изображений, будет обрабат.только те,которых нет в папке с результатом)
import newer from "gulp-newer";
// для вибора режима - разработчика или продакшн
import ifPlugin from "gulp-if";


// экспортируем объект
export const plugins = {
    replace: replace,
    plumber: plumber,
    notify: notify,
    browsersync: browsersync,
    newer: newer,
    if: ifPlugin,
}